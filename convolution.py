import torch
import torch.nn.functional as F
import numpy as np
import cv2 as cv
import matplotlib.pyplot as plt
# from matplotlib import pyplot as plt

print("OpenCV version " + cv.__version__)
print("PyTorch version" + torch.__version__ + '\n')

# Generates circular kernels of a specified radius
# Fiddle with epsilon
def circular_kernel(radius: int, filled: bool) -> np.ndarray:
    width, height = radius * 2 + 1, radius * 2 + 1
    a, b = radius, radius
    epsilon = 7
    
    kern = np.zeros((width, height), dtype=np.int32)
    for y in range(height):
        for x in range(width):
            # determine proximity to (x-a)**2 + (y-b)**2 == r**2
            if abs((x - a) ** 2 + (y - b) ** 2 - radius ** 2) < epsilon ** 2:
                kern[y][x] = 1
            if filled and abs((x - a) ** 2 + (y - b) ** 2) < radius ** 2:
                kern[y][x] = 1
    return np.array(kern)

# Reads the image as a two-dimensional numpy array
def read_image(path: str) -> np.ndarray:
    # imread() returns a 16-bit unsigned integer array, but torch.from_numpy() 
    # can only accept unsigned integers of eight bits
    img = cv.imread(path, cv.IMREAD_UNCHANGED)
    img = np.int32(img)
    if len(img.shape) == 2:
        return img
    print(path + " is not a grayscale image and will be converted to one")
    return cv.imread(path, cv.IMREAD_GRAYSCALE)

def convolve(img: np.ndarray, ker: np.ndarray):
    # Apparently we need tensors instead of arrays...my physics class still
    # hasn't explained the difference between the two
    img_tensor = torch.from_numpy(img)
    ker_tensor = torch.from_numpy(ker)
    
    # Reshape the tensors so that its axis-0 and axis-1 correspond to the
    # batch size and number of input channels respectively: squeeze squeeze
    img_tensor = torch.unsqueeze(img_tensor, 0)
    img_tensor = torch.unsqueeze(img_tensor, 0)
    ker_tensor = torch.unsqueeze(ker_tensor, 0)
    ker_tensor = torch.unsqueeze(ker_tensor, 0)
        
    padding = int((ker.shape[0] - 1) / 2)
    
    return F.conv2d(img_tensor, ker_tensor, padding = padding)

# Divides the pixels of the convolved image by those of the original image
def normalize(img: np.ndarray, conv: np.ndarray) -> np.ndarray:
    # TODO: use numpy divide
    for index in np.ndindex(img.shape):
        conv[index] = conv[index] / (img[index])
    return conv

##############################################################################

radius = 11
kernel = circular_kernel(radius, False)
image = read_image("lionheart.tif")
# kernel = np.array([[-1, -1, -1], [1, 1, 1], [1, 1, 1]], dtype=np.int32)
# kernel = np.array([[0, 0, 0], [0, 1, 0], [0, 0, 0]], dtype=np.int32)
# convolved = convolve(image, kernel).squeeze()
convolved = normalize(image, convolve(image, kernel).squeeze().numpy())
# print(type(convolved))

# print(image.shape)
# print(convolved.numpy().shape)

print("Kernel of radius " + str(radius) + ": ")
print(kernel)

f = plt.figure()

f.add_subplot(1, 2, 1)
plt.title("Original")
plt.axis("off")
plt.imshow(image, cmap="gray")
# plt.imshow(cv.cvtColor(image, cv.COLOR_BGR2RGB) )

f.add_subplot(1, 2, 2)
plt.title("Convolved")
plt.axis("off")
plt.imshow(convolved, cmap="gray")
# plt.imshow(convolved.squeeze().detach().numpy())

plt.tight_layout()
plt.show(block=True)